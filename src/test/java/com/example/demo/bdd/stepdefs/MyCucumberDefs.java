package com.example.demo.bdd.stepdefs;

import com.example.demo.model.Consultadd;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import org.junit.Assert;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;

public class MyCucumberDefs{
    RestTemplate restTemplate = new RestTemplate();
    int code;
    String url ;
    Response response;
    @Given("^user wants hits the url \"([^\"]*)\"$")
    public void user_wants_hits_the_url(String url) throws Throwable {
        System.out.println("URL: "+ url);
        this.url = url;
        this.url += "/name";
        System.out.println(this.url);
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<String> result = restTemplate.exchange(this.url, HttpMethod.GET,entity,String.class);
        System.out.println(result.getStatusCodeValue());
    }


    @When("^user perform a post request at \"([^\"]*)\" with blow details$")
    public void user_perform_a_post_request_at_with_blow_details(String url, DataTable dataTable) throws Throwable {
        System.out.println("when : "+this.url);
        List<String> list= dataTable.asList(String.class);
        Consultadd c = new Consultadd();
        long l = Long.parseLong(list.get(5));
        c.setEmpId(l) ;
        c.setName(list.get(6));
        c.setDept(list.get(7));
        c.setSalary(Integer.parseInt(list.get(8)));
        c.setPf(list.get(9));
        HttpEntity<Consultadd> request = new HttpEntity<>(c);
        System.out.println("req :"+request.getClass());
        String foo = restTemplate.postForObject(this.url, c, String.class);
        ResponseEntity<String> result = restTemplate.exchange(this.url,HttpMethod.POST,request,String.class);
        System.out.println(foo);
        System.out.println(result.getStatusCodeValue());
        code = 200;
    }


    @Then("^the request code should be (\\d+)$")
    public void the_request_code_should_be(int code) {
        System.out.println("Code:"+code);
        System.out.println("Code:"+this.code);
        Assert.assertEquals(this.code,code);
    }
}
