package com.example.demo.bdd;


import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/feature", plugin = {"pretty",
        "json:target/cucumber-report.json"})
public class CucumberTest {

}
