//import com.example.demo.bdd.CucumberTest;
//import com.jayway.restassured.http.ContentType;
//import cucumber.api.DataTable;
//import cucumber.api.PendingException;
//import cucumber.api.java.en.Given;
//import cucumber.api.java.en.Then;
//import cucumber.api.java.en.When;
//import gherkin.pickles.PickleRow;
//import io.restassured.response.Response;
//import org.junit.Assert;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.client.RestTemplate;
//import static com.jayway.restassured.RestAssured.given;
//import java.util.LinkedHashMap;
//import java.util.Map;
//
//
//public class MyStepdefs{
//    @Autowired
//    RestTemplate restTemplate;
//    int code;
//    String url ;
//    Response response;
//    @Given("^user wants hits the url \"([^\"]*)\"$")
//    public void user_wants_hits_the_url(String url) throws Throwable {
//        System.out.println("URL: "+ url);
//        this.url = url;
//        response = (Response) given().when().get(url);
//        Assert.assertEquals(200,response.getStatusCode());
//    }
//
//
//    @When("^user perform a post request at \"([^\"]*)\" with blow details$")
//    public void user_perform_a_post_request_at_with_blow_details(String url, DataTable dataTable) throws Throwable {
//        Map<String,String> body = new LinkedHashMap<String,String>();
//        this.url  =  this.url + url;
//        System.out.println(url);
//        for (PickleRow row : dataTable.getPickleRows())
//        {
//            body.put(row.getCells().get(0).toString(),row.getCells().get(1).toString());
//        }
//        response = (Response) given().contentType(ContentType.JSON).body(body).when().post(this.url);
//        code = response.getStatusCode();
//        System.out.println(code);
//
//        throw new PendingException();
//    }
//
//
//    @Then("^the request code should be (\\d+)$")
//    public void the_request_code_should_be(int code) {
//        Assert.assertEquals(this.code,code);
//    }
//
//
//}
